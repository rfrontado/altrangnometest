package com.frontado.altrangnometest.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.frontado.altrangnometest.R;
import com.frontado.altrangnometest.models.Gnome;
import com.frontado.altrangnometest.utils.PicassoCircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* Created by robertofrontado on 10/12/15.
 */
public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {

    public interface OnItemClickListener{
        void onItemClick(View view, Gnome gnome);
    }

    private OnItemClickListener mOnItemClickListener;

    List<Gnome> gnomes;
    List<Gnome> filteredGnomes;

    public Adapter(List<Gnome> gnomes){
        this.gnomes = new ArrayList<>();
        this.filteredGnomes = new ArrayList<>();
        this.gnomes.addAll(gnomes);
        this.filteredGnomes.addAll(gnomes);
    }

    public void setGnomes(List<Gnome> gnomes){
        this.gnomes.clear();
        this.filteredGnomes.clear();
        this.gnomes.addAll(gnomes);
        this.filteredGnomes.addAll(gnomes);
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder viewHolder, final int position) {
        final Gnome gnome = filteredGnomes.get(position);

        viewHolder.tvNameAndAge.setText(gnome.getName() + " (" + gnome.getAge() + ")");
        //All his professions in a single line separated by ','
        viewHolder.tvProfession.setText(gnome.getProfessionsSingleLine());

        Picasso.with(viewHolder.itemView.getContext())
                .load(gnome.getThumbnail())
                .transform(new PicassoCircleTransform())
                .into(viewHolder.ivPicture);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null)
                    mOnItemClickListener.onItemClick(v, gnome);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredGnomes.size();
    }

    @Override
    public int getItemViewType(int position){
        return position;
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mOnItemClickListener = listener;
    }

    //Filters
    public void filterByName(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        filteredGnomes.clear();
        if (charText.length() == 0)
            filteredGnomes.addAll(gnomes);
        else
        {
            for (Gnome gnome : gnomes)
            {
                if (gnome.getName()
                        .toLowerCase(Locale.getDefault())
                        .startsWith(charText))
                    filteredGnomes.add(gnome);
            }
        }
        notifyDataSetChanged();
    }

    public void filterByAge(String charText) {
        filteredGnomes.clear();
        if (charText.length() == 0)
            filteredGnomes.addAll(gnomes);
        else
        {
            for (Gnome gnome : gnomes)
            {
                if (String.valueOf(gnome.getAge())
                        .startsWith(charText))
                    filteredGnomes.add(gnome);
            }
        }
        notifyDataSetChanged();
    }

    public void filterByProfession(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        filteredGnomes.clear();
        if (charText.length() == 0)
            filteredGnomes.addAll(gnomes);
        else
        {
            for (Gnome gnome : gnomes)
            {
                if (gnome.getProfessionsSingleLine()
                        .toLowerCase(Locale.getDefault())
                        .contains(charText))
                    filteredGnomes.add(gnome);
            }
        }
        notifyDataSetChanged();
    }

    // View holder

    static class Holder extends RecyclerView.ViewHolder {

        protected TextView tvNameAndAge;
        protected TextView tvProfession;
        protected ImageView ivPicture;

        public Holder(View itemView) {
            super(itemView);
            tvNameAndAge = (TextView) itemView.findViewById(R.id.tv_item_name_age);
            tvProfession = (TextView) itemView.findViewById(R.id.tv_item_profession);
            ivPicture = (ImageView) itemView.findViewById(R.id.iv_item_picture);
        }
    }
}
