package com.frontado.altrangnometest.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.frontado.altrangnometest.R;
import com.frontado.altrangnometest.adapters.Adapter;
import com.frontado.altrangnometest.models.Gnome;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public enum Filters {
        NAME,
        AGE,
        PROFESSION
    }

    Adapter adapter;
    List<Gnome> gnomes;
    Filters currentFilter = Filters.NAME;
    TextView tvNoGnomes;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Brastlewark gnomes");
        toolbar.setTitleTextColor(Color.WHITE);

        gnomes = new ArrayList<>();

        tvNoGnomes = (TextView) findViewById(R.id.tv_main_no_results);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new Adapter(gnomes);
        adapter.setOnItemClickListener(new Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Gnome gnome) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(Gnome.EXTRA_GNOME, gnome);
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,
                        view.findViewById(R.id.iv_item_picture), "picture");
                startActivity(intent, options.toBundle());
            }
        });
        recyclerView.setAdapter(adapter);

        Gnome.getGnomes(MainActivity.this, new Gnome.GnomeListListener() {
            @Override
            public void onSuccess(List<Gnome> gnomesList) {
                gnomes.addAll(gnomesList);
                adapter.setGnomes(gnomes);
                recyclerView.setVisibility(View.VISIBLE);
                tvNoGnomes.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                Toast.makeText(MainActivity.this, "Error de conexión", Toast.LENGTH_SHORT).show();
                recyclerView.setVisibility(View.GONE);
                tvNoGnomes.setVisibility(View.VISIBLE);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search_button).getActionView();

        // Changing the searchView's text color
        SearchView.SearchAutoComplete theTextArea = (SearchView.SearchAutoComplete)searchView.findViewById(R.id.search_src_text);
        theTextArea.setTextColor(Color.WHITE);

        if (searchView != null) {
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getComponentName()));

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    switch (currentFilter) {
                        case NAME:
                            adapter.filterByName(newText);
                            break;
                        case AGE:
                            adapter.filterByAge(newText);
                            break;
                        case PROFESSION:
                            adapter.filterByProfession(newText);
                            break;
                    }
                    if (adapter.getItemCount() == 0) {
                        recyclerView.setVisibility(View.GONE);
                        tvNoGnomes.setVisibility(View.VISIBLE);
                    } else {
                        recyclerView.setVisibility(View.VISIBLE);
                        tvNoGnomes.setVisibility(View.GONE);
                    }

                    return false;
                }
            });
        }

        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.filter_name) {
            currentFilter = Filters.NAME;
        } else if (id == R.id.filter_age) {
            currentFilter = Filters.AGE;
        } else if (id == R.id.filter_profession) {
            currentFilter = Filters.PROFESSION;
        }

        return super.onOptionsItemSelected(item);
    }
}
