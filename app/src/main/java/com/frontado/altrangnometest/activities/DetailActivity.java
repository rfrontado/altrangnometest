package com.frontado.altrangnometest.activities;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.frontado.altrangnometest.R;
import com.frontado.altrangnometest.models.Gnome;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Gnome's detail");
        toolbar.setTitleTextColor(Color.WHITE);

        if (getIntent().getSerializableExtra(Gnome.EXTRA_GNOME) != null) {
            Gnome gnome = (Gnome) getIntent().getSerializableExtra(Gnome.EXTRA_GNOME);

            ImageView ivPicture = (ImageView)findViewById(R.id.iv_detail_picture);

            TextView tvName = (TextView)findViewById(R.id.tv_detail_name);
            TextView tvAge = (TextView)findViewById(R.id.tv_detail_age);
            TextView tvWeight = (TextView)findViewById(R.id.tv_detail_weight);
            TextView tvHeight = (TextView)findViewById(R.id.tv_detail_height);
            TextView tvHairColor = (TextView)findViewById(R.id.tv_detail_hair_color);
            TextView tvProfessions = (TextView)findViewById(R.id.tv_detail_professions);
            TextView tvFriends = (TextView)findViewById(R.id.tv_detail_friends);

            tvName.setText(gnome.getName());
            tvAge.setText(String.valueOf(gnome.getAge()));
            tvWeight.setText((int)gnome.getWeight() + " kg");
            tvHeight.setText((int)gnome.getHeight() + " cm");
            tvHairColor.setText(gnome.getHairColor());
            tvProfessions.setText(gnome.getProfessionsSingleLine());
            tvFriends.setText(gnome.getFriendsSingleLine());

            Picasso.with(DetailActivity.this)
                    .load(gnome.getThumbnail())
                    .into(ivPicture);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Reverse material animations
        if (id == android.R.id.home) {
            supportFinishAfterTransition();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
