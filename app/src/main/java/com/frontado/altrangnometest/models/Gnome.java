package com.frontado.altrangnometest.models;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by robertofrontado on 10/12/15.
 */
public class Gnome implements Serializable {

    public static String EXTRA_GNOME = "GNOME";

    public interface GnomeListListener {
        void onSuccess(List<Gnome> gnomesList);
        void onError();
    }

    //This attributes names must match with the json object
    private int id;
    private String name;
    private String thumbnail;
    private int age;
    private float weight;
    private float height;
    private String hair_color;
    private String[] professions;
    private String[] friends;

    public static void getGnomes(Context context, final GnomeListListener listener){

        String url = "https://raw.githubusercontent.com/AXA-GROUP-SOLUTIONS/mobilefactory-test/master/data.json";

        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    //Get the Brastlewark object
                    JSONArray responseObject = object.getJSONArray("Brastlewark");
                    GsonBuilder builder = new GsonBuilder();
                    Gson gson = builder.create();
                    //Make gson able to parse a list of gnomes
                    Type listType = new TypeToken<List<Gnome>>(){}.getType();
                    //Parse the json
                    List<Gnome> gnomes = gson.fromJson(responseObject.toString(), listType);
                    listener.onSuccess(gnomes);

                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError();
                }
                catch (IllegalStateException e) {
                    e.printStackTrace();
                    listener.onError();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
                listener.onError();
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        Volley.newRequestQueue(context).add(jsonObjectRequest);
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public int getAge() {
        return age;
    }

    public float getWeight() {
        return weight;
    }

    public float getHeight() {
        return height;
    }

    public String getHairColor() {
        return hair_color;
    }

    public String[] getProfessions() {
        return professions;
    }

    public String[] getFriends() {
        return friends;
    }

    //Setters
    //In this case I didn't need setters

    //Methods
    public String getProfessionsSingleLine(){

        if(professions.length == 0)
            return "I'm still looking for something to do :)!";

        String professionsSingleLine = "";
        for (String profession : professions) {
            // Check if it's the first one
            if(profession == professions[0])
                professionsSingleLine += profession;
            else
                professionsSingleLine += ", " + profession;
        }
        return professionsSingleLine;
    }

    public String getFriendsSingleLine(){

        if(friends.length == 0)
            return "I'm friendly I swear :)!";

        String friendsSingleLine = "";
        for (String friend : friends) {
            // Check if it's the first one
            if(friend == friends[0])
                friendsSingleLine += friend;
            else
                friendsSingleLine += ", " + friend;
        }
        return friendsSingleLine;
    }
}
